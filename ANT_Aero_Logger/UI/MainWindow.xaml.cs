﻿using System;
using System.Collections.Generic;
using System.Windows;
using ANT_Managed_Library;      //Reference the ANT_Managed_Library namespace to make the code easier and more readable
using System.IO;
using System.Linq;
using System.Timers;

namespace ANT_Aero_Logger
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    //This delegate is for using the dispatcher to avoid conflicts of the feedback thread referencing the UI elements
    delegate void dAppendText(String toAppend);
    static readonly object _locker = new object();
    private bool _velo_connected = false;
    public bool run_in_progress = false;
    public bool setpoint_in_progress = false;
    private static System.Timers.Timer _setpt_timer;
    private static System.Timers.Timer _setpt_timer_text_update;
    private static DateTime _setpt_start_time;
    private static float _setpt_update_time_ms = 100;

    static MainWindow frm;

    public MainWindow()
    {
      InitializeComponent();
      frm = this;
      //Set frequencies:
      this.cmbPeriod.Items.Add("8192");
      this.cmbPeriod.Items.Add("4096");
      this.cmbPeriod.SelectedIndex = 0;

      //Calibration_Test.RunTest();
    }

    public struct VeloData
    {
      public UInt16 tdk1_const0;
      public UInt16 tdk1_const1;
      public UInt16 tdk1_const2;
      public UInt16 tdk1_const3;
      public UInt16 tdk2_const0;
      public UInt16 tdk2_const1;
      public UInt16 tdk2_const2;
      public UInt16 tdk2_const3;
      public UInt32 tdk1_p;
      public UInt16 tdk1_t;
      public UInt32 tdk2_p;
      public UInt16 tdk2_t;
      public byte latest_page;
    }

    #region Declarations
    //ANT device object:
    ANT_Device device0;
    static readonly byte[] ant_plus_network_key = { 0xB9, 0xA5, 0x21, 0xFB, 0xBD, 0x72, 0xC3, 0x45 };  //ANT+ public key
    static readonly byte[] ant_public_network_key = { 0xE8, 0xE4, 0x21, 0x3B, 0x55, 0x7A, 0x67, 0xC1 };  //ANT+ public key
    static UInt32 packets_received = 0;
    static UInt32 packets_dropped = 0;
    StreamWriter ant_sw;
    StreamWriter sw;
    VeloData velo_data = new VeloData();
    List<UInt16> connected_devices = new List<ushort>();
    #endregion Declarations

    #region Dispatch Functions
    public static void AppendMessageBox(string strOut)
    {
      if (!frm.Dispatcher.CheckAccess())
      {
        frm.Dispatcher.Invoke(new Action<string>(AppendMessageBox), new object[] { strOut });
        return;
      }
      frm.txtMessages.AppendText(strOut + "\n");
      frm.txtMessages.ScrollToEnd();
    }

    public static void UpdateVelosenseDataBox(string strOut)
    {
      if (!frm.Dispatcher.CheckAccess())
      {
        frm.Dispatcher.Invoke(new Action<string>(UpdateVelosenseDataBox), new object[] { strOut });
        return;
      }
      frm.txtVeloData.Text = strOut;
    }

    public static void UpdateSetpointButtonText(string str)
    {
      if (!frm.Dispatcher.CheckAccess())
      {
        frm.Dispatcher.Invoke(new Action<string>(UpdateSetpointButtonText), new object[] { str });
        return;
      }
      frm.butSetpoint.Content = str;
    }

    public static void UpdateSetpointButtonBackground(System.Windows.Media.SolidColorBrush color)
    {
      if (!frm.Dispatcher.CheckAccess())
      {
        frm.Dispatcher.Invoke(new Action<System.Windows.Media.SolidColorBrush>(UpdateSetpointButtonBackground), new object[] { color });
        return;
      }
      frm.butSetpoint.Background = color;
    }

    public static uint GetPeriod()
    {
      string velo_rate = "";
      if (!frm.Dispatcher.CheckAccess())
      {
        frm.Dispatcher.Invoke(new Action(() =>
        {
          velo_rate = frm.cmbPeriod.Text;
        }));
      }
      else { velo_rate = frm.cmbPeriod.Text; }
      uint result = Convert.ToUInt16(velo_rate);
      return result;
    }

    public static string GetDescription()
    {
      string description = "";
      if (!frm.Dispatcher.CheckAccess())
      {
        frm.Dispatcher.Invoke(new Action(() =>
        {
          description = frm.txtDescription.Text;
        }));
      }
      else description = frm.txtDescription.Text;
      return description;
    }

    public string GetRootDir()
    {
      string root_dir = "";
      if (!Dispatcher.CheckAccess())
      {
        Dispatcher.Invoke(new Action(() =>
        {
          root_dir = txtRootDir.Text;
        }));
      }
      else { root_dir = txtRootDir.Text; }
      return root_dir;
    }

    public float GetSampleTime()
    {
      string sample_time = "";
      if (!Dispatcher.CheckAccess())
      {
        Dispatcher.Invoke(new Action(() =>
        {
          sample_time = txtRootDir.Text;
        }));
      }
      else { sample_time = txtSampleTime.Text; }
      return Convert.ToSingle(sample_time);
    }

    public string GetRunText()
    {
      string runno = "";
      if (!Dispatcher.CheckAccess())
      {
        Dispatcher.Invoke(new Action(() =>
        {
          runno = txtRun.Text;
        }));
      }
      else { runno = txtRun.Text; }
      return runno;
    }

    public void UpdateGrid()
    {
      if (!Dispatcher.CheckAccess())
      {
        frm.Dispatcher.Invoke(new Action(UpdateGrid));
        return;
      }

      frm.gridSetpoints.ItemsSource = Data_logger.setpoint_data;
      frm.gridSetpoints.Items.Refresh();
    }

    public static void RunTxtEnable(bool IsEnabled)
    {
      if (!frm.Dispatcher.CheckAccess())
      {
        frm.Dispatcher.Invoke(new Action<bool>(RunTxtEnable), new object[] { IsEnabled });
        return;
      }
      frm.txtRun.IsEnabled = IsEnabled;
    }

    #endregion Dispatch Functions

    #region Events

    private void ButOpenConnection_Click(object sender, RoutedEventArgs e)
    {
      if (_velo_connected)
      {
        VeloListener.Shutdown();
        AppendMessageBox("Velo disconnected.");
        butOpenConnection.Content = "Connect";
        butOpenConnection.Background = System.Windows.Media.Brushes.LightGreen;
        _velo_connected = false;
      }
      else
      {
        AppendMessageBox("Opening connection.");
        this.UpdateLayout();
        if (VeloListener.OpenChannel())
        {
          AppendMessageBox("Velo connected");
          butOpenConnection.Content = "Disconnect";
          butOpenConnection.Background = System.Windows.Media.Brushes.Crimson;
          _velo_connected = true;
        }
        else
        {
          AppendMessageBox("Velo connection failed.");
          butOpenConnection.Content = "Connect";
          butOpenConnection.Background = System.Windows.Media.Brushes.LightGreen;
          _velo_connected = false;
        }
      }
    }

    private void ButGetRunNo_Click(object sender, RoutedEventArgs e)
    {
      CheckNextRunNo();
    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      if (_velo_connected)
      {
        VeloListener.Shutdown();
      }
    }

    private void ButStartLog_Click(object sender, RoutedEventArgs e)
    {
      if (_velo_connected)
      {
        // Check run is correct.
        if (!run_in_progress)
        {
          frm.CheckNextRunNo();
        }
        StartStopRun();
        if (run_in_progress)
        {
          AppendMessageBox($"Run {txtRun.Text} started..");
          butStartLog.Background = System.Windows.Media.Brushes.Crimson;
          butStartLog.Content = "Stop";
        }
        else
        {
          AppendMessageBox($"Run {txtRun.Text} stopped..");
          butStartLog.Background = System.Windows.Media.Brushes.LightGreen;
          butStartLog.Content = "Start";
          frm.CheckNextRunNo();

        }
      }
      else
      {
        AppendMessageBox("Unable to start run because sensor isn't connected.");
      }
    }


    private void ButSetpoint_Click(object sender, RoutedEventArgs e)
    {
      if (_velo_connected & run_in_progress)
      {
        StartStopSetpoint();
      }
    }
    #endregion Events

    #region Helper functions

    public void UpdateRunNumber(string strOut)
    {
      if (!Dispatcher.CheckAccess())
      {
        Dispatcher.Invoke(new Action<string>(UpdateRunNumber), new object[] { strOut });
        return;
      }
      this.txtRun.Text = strOut;
    }

    private void EnableRunTxt(bool IsEnabled)
    {
      frm.txtRun.IsEnabled = IsEnabled;
    }

    public short GetNextRunNumber()
    {
      short next_run_no = 0;
      short req_run_no = 0;
      if (!Int16.TryParse(GetRunText(), out req_run_no)) req_run_no = 0;
      string[] log_files = Directory.GetFiles(GetRootDir(), "*.csv").Select(Path.GetFileName).ToArray();
      // Gets next run number by looking for last run number in log names.

      for (int i = 0; i < log_files.Length; i++)
      {
        string[] parts = log_files[i].Split('_');
        short run_no;
        bool result = short.TryParse(parts[1], out run_no);
        if (result)
        {
          if (run_no > next_run_no)
          {
            next_run_no = run_no;
          }
        }
      }
      next_run_no++;
      if (req_run_no > next_run_no) next_run_no = req_run_no;
      return next_run_no;
    }

    public void CheckNextRunNo()
    {
      UpdateRunNumber(GetNextRunNumber().ToString());
    }


    public static void StartStopRun(bool StopOnly = false)
    {

      if (frm.run_in_progress | StopOnly)
      {
        frm.run_in_progress = false;
        Data_logger.StopLogging();
        RunTxtEnable(true);
      }
      else
      {
        frm.run_in_progress = true;
        //Generate file root.
        string file_root = frm.txtRootDir.Text;
        if (file_root.Substring(file_root.Length - 1) != "/") { file_root += "/"; }
        file_root += "Run_" + frm.GetNextRunNumber().ToString("000");
        Data_logger.StartLogging(file_root, GetDescription());
        RunTxtEnable(false);
      }
    }



    #endregion Helper functions
    private static void StartStopSetpoint()
    {
      if (!frm.setpoint_in_progress)
      {
        _start_setpoint();
        AppendMessageBox($"Setpoint {Data_logger.CurrentSetpointNo} started..");
        frm.setpoint_in_progress = true;
      }
      else
      {
        _setpt_timer.Stop();
        _stop_setpoint();
      }
    }

    private static void _start_setpoint()
    {
      float sample_time = frm.GetSampleTime();

      _setpt_timer = new System.Timers.Timer();
      _setpt_timer.AutoReset = false;
      _setpt_timer.Elapsed += _stop_setpoint_cb;
      _setpt_timer.Interval = sample_time * 1000;

      Data_logger.StartSetpt();

      _setpt_timer.Start();

      _setpt_start_time = DateTime.Now;

      _setpt_timer_text_update = new Timer();
      _setpt_timer_text_update.AutoReset = true;
      _setpt_timer_text_update.Elapsed += _update_setpoint_button;
      _setpt_timer_text_update.Interval = _setpt_update_time_ms;
      _setpt_timer_text_update.Start();

      frm.butSetpoint.Background = System.Windows.Media.Brushes.Orchid;
    }

    private static void _update_setpoint_button(Object source, ElapsedEventArgs e)
    {
      if (frm.setpoint_in_progress)
      {
        string str = $"Stop@{(DateTime.Now - _setpt_start_time).TotalSeconds,5:F1}s";
        UpdateSetpointButtonText(str);
      }
    }
    private static void _stop_setpoint_cb(Object source, ElapsedEventArgs e)
    {
      _stop_setpoint();
    }

    private static void _stop_setpoint()
    {
      _setpt_timer_text_update.Stop();
      frm.setpoint_in_progress = false;
      Data_logger.StopSetpt();
      UpdateSetpointButtonBackground(System.Windows.Media.Brushes.LightGreen);
      UpdateSetpointButtonText("Start Setpoint");
      AppendMessageBox($"Setpoint {Data_logger.CurrentSetpointNo} stopped..");
      frm.UpdateGrid();
    }

    private void ButGetDir_Click(object sender, RoutedEventArgs e)
    {
      Velo_Raw_Extractor.ConvertAntToRaw(GetRootDir());
    }
  }
}
