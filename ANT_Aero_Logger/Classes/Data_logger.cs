﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ANT_Aero_Logger
{
  public class Setpoint
  {
    public int setpoint { get; set; }
    public float temp { get; set; }
    public float baro { get; set; }
    public float yaw { get; set; }
    public float pdyn { get; set; }
    public float dp1 { get; set; }
    public float dp2 { get; set; }
    public int samples{get;set;}
  }

  public class av_data
  {
    public double pdyn;
    public double yaw;
    public double temp;
    public double baro;
    public double dp1;
    public double dp2;
    public int nsamples;
    public int setpoint;

    public av_data()
    {
      reset_av_data(true);
    }

    public void reset_av_data(bool reset_setpt = false)
    {
      pdyn = 0;
      yaw = 0;
      temp = 0;
      baro = 0;
      dp1 = 0;
      dp2 = 0;
      nsamples = 0;
      if (reset_setpt) setpoint = 0;
    }
  }

  public class RunSetpoints : List<Setpoint>
  {
    public void add_row(av_data avdat)
    {
      this.Insert(0, new Setpoint()
      {
        setpoint = avdat.setpoint,
        pdyn = (float)avdat.pdyn,
        yaw = (float)avdat.yaw,
        temp = (float)avdat.temp,
        baro = (float)avdat.baro,
        dp1= (float)avdat.dp1,
        dp2 = (float)avdat.dp2,
        samples = avdat.nsamples,
      });
    }
  }
  static public class Data_logger
  {
    private static bool _logging = false;
    private static bool _sampling_setpt = false;
    private static StreamWriter _setpt_writer;
    private static StreamWriter _ant_writer;
    private static StreamWriter _eng_writer;

    private static string _setpt_file;
    private static string _ant_file;
    private static string _eng_file;
    private static DateTime startTime;
    public static RunSetpoints setpoint_data = new RunSetpoints();

    static readonly object _locker = new object();


    private static av_data _av_data= new av_data();

    public static int CurrentSetpointNo
    {
      get
      {
        return _av_data.setpoint;
      }
    }


    public static void StartSetpt()
    {
      if (!_sampling_setpt)
      {
        _av_data.reset_av_data();
        _sampling_setpt = true;
      }
    }

    public static void LogSetptData(VeloData vd)
    {
      lock (_locker)
      {
        if (_sampling_setpt)
        {
          //_av_data.pdyn += vd.pdyn;
          //_av_data.yaw += vd.yaw;
          //_av_data.temp += vd.temp;
          //_av_data.baro += vd.baro;
          //_av_data.dp1 += vd.p_sdp_1;
          //_av_data.dp2 += vd.p_sdp_2;
          _av_data.nsamples++;
        }
      }
    }

    public static void StopSetpt()
    {
      lock (_locker)
      {
        _av_data.pdyn /= _av_data.nsamples;
        _av_data.yaw /= _av_data.nsamples;
        _av_data.temp /= _av_data.nsamples;
        _av_data.baro /= _av_data.nsamples;
        _av_data.dp1 /= _av_data.nsamples;
        _av_data.dp2 /= _av_data.nsamples;
        //Write data:
        string str = $"{_av_data.setpoint}";
        str += $",{_av_data.pdyn:F1}";
        str += $",{_av_data.yaw:F1}";
        str += $",{_av_data.baro:F1}";
        str += $",{_av_data.temp:F1}";
        float density = 0;// Velo_Calibration.CalcDensityFromInputs((float)_av_data.temp, (float)_av_data.baro);
        str += $",{density:F3}";
        float airspeed = 0;//Velo_Calibration.CalcAirSpeedFromInputs((float)_av_data.pdyn, density);
        str += $",{airspeed:F1}";
        str += $",{_av_data.nsamples}";
        _setpt_writer.WriteLine(str);
        _sampling_setpt = false;
        setpoint_data.add_row(_av_data);
        _av_data.setpoint++;
      }
    }


    public static bool StartLogging(string file_root, string description)
    {
      if (!_start_setpt_file(file_root, description)) return false;
      if (!_start_ant_file(file_root)) return false;
      if (!_start_eng_file(file_root)) return false;
      startTime = DateTime.Now;
      _logging = true;
      //Reset data:
      setpoint_data = new RunSetpoints();
      _av_data.reset_av_data(true);
      return true;
    }

    public static bool StopLogging()
    {
      _logging = false;
      _setpt_writer.Flush();
      _setpt_writer.Close();

      _ant_writer.Flush();
      _ant_writer.Close();

      _eng_writer.Flush();
      _eng_writer.Close();
      return true;
    }

    private static bool _start_setpt_file(string file_root, string description)
    {
      try
      {
        _setpt_file = file_root + "_setpt.csv";
        _setpt_writer = new StreamWriter(_setpt_file);

        _setpt_writer.WriteLine($"Description,{MainWindow.GetDescription()}");
        string data = "Setpt,pdyn,yaw,pbaro,temp,density,airspeed,samplecount";
        _setpt_writer.WriteLine(data);
      }
      catch
      {
        return false;
      }
      return true;
    }

    private static bool _start_ant_file(string file_root)
    {
      try
      {
        _ant_file = file_root + "_ant.csv";
        _ant_writer = new StreamWriter(_ant_file);
        string data = "Timestamp,b0,b1,b2,b3,b4,b5,b6,b7";
        _ant_writer.WriteLine(data);
      }
      catch
      {
        return false;
      }
      return true;
    }

    private static bool _start_eng_file(string file_root)
    {
      try
      {
        _eng_file = file_root + "_eng.csv";
        _eng_writer = new StreamWriter(_eng_file);
        string data = "Timestamp,pdyn,yaw,pbaro,temp,density,airspeed,setpoint, logging";
        _eng_writer.WriteLine(data);
      }
      catch
      {
        return false;
      }
      return true;
    }

    public static bool LogPacket(DateTime timestamp, byte[] packet)
    {
      if (_logging)
      {
        string str=(timestamp - startTime).TotalSeconds.ToString("0.000");
        for (int i=0;i<8;i++)
        {
          str += $",{packet[i]}";
        }
        _ant_writer.WriteLine(str);
      }
      return true;
    }

    public static bool LogEngData(VeloData vd)
    {
      if (_logging)
      {
        //string str = (vd.timestamp-startTime).TotalSeconds.ToString("0.000");
        //str += $",{vd.pdyn:F1}";
        //str += $",{vd.yaw:F1}";
        //str += $",{vd.baro:F1}";
        //str += $",{vd.temp:F1}";
        //str += $",{vd.density:F3}";
        //str += $",{vd.airspeed:F1}";
        //str += $",{_av_data.setpoint}";
        //str +=$",{_sampling_setpt}";
        //_eng_writer.WriteLine(str);
      }
      return true;
    }


  }
}
