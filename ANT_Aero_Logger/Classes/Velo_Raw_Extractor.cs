﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ANT_Aero_Logger
{
  public class AntData
  {
    public byte[] payload;
    public float timeStamp;
    public AntData()
    {
      payload = new byte[8];
    }

  }
  public static class Velo_Raw_Extractor
  {
    public static void ConvertAntToRaw(string path)
    {
      // List files ant files.
      List<string> ant_file_list = new List<string>();
      string[] all_files = Directory.GetFiles(path);
      foreach (string file in all_files)
      {
        string rawFile = file.Substring(0, file.Length - 8) + "_raw.csv";
        string engFile = file.Substring(0, file.Length - 8) + "_eng.csv";
        string setptFile = file.Substring(0, file.Length - 8) + "_setpt_recal_oct19.csv";
        if ((file.Substring(file.Length - 8, 8) == "_ant.csv") && (File.Exists(engFile)))
        {
          List<AntData> antData = antFileData(file);
          List<VeloData> decdat = DecodeRawData(antData);
          // Write output data:
          WriteOutputRawData(rawFile, engFile, setptFile, decdat);

        }
      }

    }

    public struct eng_setpt_def
    {
      public float time;
      public uint setpt;
      public bool onsetpt;
    }

    public static void WriteOutputRawData(string rawFile, string engFile, string setptFile, List<VeloData> data)
    {
      StreamWriter swraw = new StreamWriter(rawFile);
      StreamWriter swsetpt = new StreamWriter(setptFile);
      // header:
      string str = "time,p1,p2,tdk1,tdk2,temp,baro,pdyn,yaw,baro_cor,density,airspeed";
      swraw.WriteLine(str);
      str = "setpt,p1,p2,tdk1,tdk2,temp,baro,pdyn,yaw,baro_cor,density,airspeed,NSamples";
      swsetpt.WriteLine(str);
      bool onSetpt = false;
      uint count = 0;
      // assume raw data and eng data files line up.
      // get eng data setpoints:
      string[] setdef = new string[2];
      List<eng_setpt_def> setdefs = new List<eng_setpt_def>();
      // Get setpoint definitions:
      //setdefs = setptDefs(engFile);
      float[] setptdata = new float[13];
      int index = 0;
      for (int i=0; i<12;i++)
      {
        setptdata[i] = 0.0f;
      }
      foreach (VeloData vd in data)
      {
        //str = $"{vd.timeFromStart:F3}";
        //str += $",{vd.p_sdp_1:F6}";
        //str += $",{vd.p_sdp_2:F6}";
        //str += $",{vd.p_tdk_1:F6}";
        //str += $",{vd.p_tdk_2:F6}";
        //str += $",{vd.temp:F6}";
        //str += $",{vd.baro:F6}";
        //str += $",{vd.pdyn:F6}";
        //str += $",{vd.yaw:F6}";
        //str += $",{vd.baro_cor:F6}";
        //str += $",{vd.density:F6}";
        //str += $",{vd.airspeed:F6}";
        //swraw.WriteLine(str);
        //// Rubbish, but we don't do it often.  just easier to do this way.
        //index = -1;
        //for (int i = 0; i < setdefs.Count; i++)
        //  if (Math.Abs(setdefs[i].time - vd.timeFromStart) < 0.01)
        //    index = i;
        //if (index < 0) continue;
        //if (setdefs[index].onsetpt)
        //{
        //  onSetpt = true;
        //  setptdata[1] += vd.p_sdp_1;
        //  setptdata[2] += vd.p_sdp_2;
        //  setptdata[3] += vd.p_tdk_1;
        //  setptdata[4] += vd.p_tdk_2;
        //  setptdata[5] += vd.temp;
        //  setptdata[6] += vd.baro;
        //  setptdata[7] += vd.pdyn;
        //  setptdata[8] += vd.yaw;
        //  setptdata[9] += vd.baro_cor;
        //  setptdata[10] += vd.density;
        //  setptdata[11] += vd.airspeed;
        //  setptdata[12]++;
        //}
        //else
        //{
        //  if (onSetpt)
        //  {
        //    onSetpt = false;
        //    for (int i = 1; i < 12; i++)
        //    {
        //      setptdata[i] /= setptdata[12];
        //    }
        //    str = $"{setptdata[0]}";
        //    for (int i = 1; i <= 11; i++)
        //    {
        //      str += $",{setptdata[i]:F3}";
        //    }
        //    str += $",{setptdata[12]}";
        //    swsetpt.WriteLine(str);

        //    setptdata[0]++;
        //    for (int i=1;i<=12;i++)
        //    {
        //      setptdata[i] = 0;
        //    }
        //  }
        //}
      }
      swraw.Close();
      swsetpt.Close();
    }

    public static List<eng_setpt_def> setptDefs(string filePath)
    {
      StreamReader sreng = new StreamReader(filePath);
      List<eng_setpt_def> ret = new List<eng_setpt_def>();
      eng_setpt_def setdef;
      // Discard header:
      string line =sreng.ReadLine();
      while (!sreng.EndOfStream)
      {
        line = sreng.ReadLine();
        string[] elements = line.Split(',');
        setdef.time = Convert.ToSingle(elements[0]);
        setdef.setpt = Convert.ToUInt16(elements[7]);
        if (elements[8].Length > 0) setdef.onsetpt = Convert.ToBoolean(elements[8]);
        else setdef.onsetpt = false;
        ret.Add(setdef);
      }
      return ret;
    }


    public static List<AntData> antFileData(string filePath)
    {
      List<AntData> ret = new List<AntData>();
      StreamReader file_input = new StreamReader(filePath);
      // Discard header:
      string line = file_input.ReadLine();
      while (!file_input.EndOfStream)
      {
        line = file_input.ReadLine();
        string[] elements = line.Split(',');
        AntData ant_line = new AntData();
        if (elements.Length >= 9)
        {
          ant_line.timeStamp = Convert.ToSingle(elements[0]);
          for (int i = 0; i < 8; i++)
          {
            ant_line.payload[i] = Convert.ToByte(elements[i + 1]);
          }
          ret.Add(ant_line);
        }
      }
      return ret;
    }

    public static List<VeloData> DecodeRawData(List<AntData> input)
    {
      List<VeloData> ret = new List<VeloData>();
      VeloData velo_data = new VeloData();
      //foreach (AntData dat in input)
      //{
      //  //decode by page:
      //  velo_data.latest_page = dat.payload[0];
      //  switch (dat.payload[0])
      //  {
      //    case 10:
      //      velo_data.p_sdp_1 = (BitConverter.ToUInt16(dat.payload, 2) - 32768f) / 60f;
      //      velo_data.p_sdp_2 = (BitConverter.ToUInt16(dat.payload, 4) - 32768f) / 60f;
      //      velo_data.temp = BitConverter.ToUInt16(dat.payload, 6) / 100f - 30f;
      //      velo_data.p10_received = true;
      //      velo_data.p10_received_this_cycle = true;
      //      velo_data.timeFromStart = dat.timeStamp;
      //      break;
      //    case 11:
      //      velo_data.p_tdk_1 = (dat.payload[4] << 16 | dat.payload[3] << 8 | dat.payload[2]) * 0.005f + 30000f;
      //      velo_data.p_tdk_2 = (dat.payload[7] << 16 | dat.payload[6] << 8 | dat.payload[5]) * 0.005f + 30000f;
      //      velo_data.p11_received = true;
      //      velo_data.p11_received_this_cycle = true;
      //      break;
      //    case 12:
      //      velo_data.bat_level = dat.payload[2];
      //      velo_data.bat_voltage = BitConverter.ToUInt16(dat.payload, 3) / 1000f;
      //      velo_data.p12_received = true;
      //      velo_data.p12_received_this_cycle = true;
      //      break;
      //  }

      //  // Calculate data:
      //  if ((velo_data.p10_received && velo_data.p11_received) &
      //      (velo_data.p10_received_this_cycle &
      //      (velo_data.p11_received_this_cycle | velo_data.p12_received_this_cycle)))
      //  {
      //    velo_data.p10_received_this_cycle = false;
      //    velo_data.p11_received_this_cycle = false;
      //    velo_data.p12_received_this_cycle = false;
      //    ret.Add(velo_data);
      //    velo_data = new VeloData();
      //  }
      //}
      return ret;
    }

  }
}
