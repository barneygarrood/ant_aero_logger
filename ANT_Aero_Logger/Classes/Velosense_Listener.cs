﻿using System;
using ANT_Managed_Library;      //Reference the ANT_Managed_Library namespace to make the code easier and more readable
using System.IO;
using System.Collections.Generic;

namespace ANT_Aero_Logger
{
  public struct AntMessage
  {
    public DateTime time_received;
    public byte[] payload;
  }


  static class VeloListener
  {
    /// <summary>
    /// Ant listener and decoder
    /// </summary>
    /// 
    #region Declarations
    //ANT device object:
    static ANT_Device device0;
    static readonly byte[] ant_public_network_key = { 0xE8, 0xE4, 0x21, 0x3B, 0x55, 0x7A, 0x67, 0xC1 };  //ANT public key
    static readonly byte[] ant_plus_network_key = { 0xB9, 0xA5, 0x21, 0xFB, 0xBD, 0x72, 0xC3, 0x45 };  //ANT+ key
    const byte ant_freq_offset = 57;
    const ushort ant_device_no = 0;
    const byte ant_device_type = 46;
    const byte ant_transmission_type = 0;

    static DateTime latest_packet_time;
    static UInt32 packets_received = 0;
    static UInt32 packets_dropped = 0;
    static VeloData velo_data = new VeloData();
    static readonly object _locker = new object();

    private static bool _on_setpoint = false;
    private static int _setpoint_number;
    private static double _angle = 0;

    private static bool _logging = false;
    private static StreamWriter _log_writer;

    private const UInt16 screen_update_ms = 100;
    private static System.Timers.Timer _timer = new System.Timers.Timer();

    #endregion Declarations

    // Resets static variables on start connection
    public static void ResetData()
    {
      velo_data.Reset();
      packets_received = 0;
      packets_dropped = 0;
    }


    #region ANT specific functions

    public static bool OpenChannel()
    {
      try
      {
        ResetData();
        device0 = new ANT_Device();

        //set network key:
        device0.setNetworkKey(0, ant_plus_network_key);

        //Add handler function for device0 response event:
        device0.deviceResponse += new ANT_Device.dDeviceResponseHandler(Device0_deviceResponse);
        device0.getChannel(0).channelResponse += new dChannelResponseHandler(D0channel0_channelResponse);
        MainWindow.AppendMessageBox("Device 0 Connected" + Environment.NewLine);

        //Setup ANT channel:
        ANT_Channel channel0 = device0.getChannel(0);
        if (channel0.assignChannel(ANT_ReferenceLibrary.ChannelType.BASE_Slave_Receive_0x00, 0, 500))
          MainWindow.AppendMessageBox("Channel assigned on network 0.");
        else
          throw new Exception("Channel assignment failed.");

        //Set frequency:
        channel0.setChannelFreq(ant_freq_offset);
        MainWindow.AppendMessageBox("Frequency set");

        //Set period:
        ushort ant_period = (ushort)MainWindow.GetPeriod();
        channel0.setChannelPeriod(ant_period, 500);
        MainWindow.AppendMessageBox(String.Format("Period set to {0}", ant_period));

        //Set channel ID:
        channel0.setChannelID(deviceNumber: ant_device_no, 
                              pairingEnabled:false, 
                              deviceTypeID: ant_device_type, 
                              transmissionTypeID: ant_transmission_type);
        MainWindow.AppendMessageBox("Channel ID set");
        //channel0.setLowPrioritySearchTimeout(0xFF);
        //channel0.setChannelSearchTimeout(0x00);

        //Open the channel:
        if (channel0.openChannel(500))
        {
          System.Threading.Thread.Sleep(50);
          ANT_Response IDresp = device0.requestMessageAndResponse(ANT_ReferenceLibrary.RequestMessageID.CHANNEL_ID_0x51, 500);
          MainWindow.AppendMessageBox(String.Format("Opened channel ID = Dev#: {0:X}", ((IDresp.messageContents[2] << 8) + IDresp.messageContents[1])));
        }
        // rest velo data:
        _timer.AutoReset = false;
        _timer.Interval = (screen_update_ms);
        _timer.Start();
      }
      catch (Exception ex)
      {
        MainWindow.AppendMessageBox("Error: " + ex.Message);
        return false;
      }
      return true;
    }



    //Print the device response to the textbox.
    //This is just things like connection messages.
    static void Device0_deviceResponse(ANT_Response response)
    {
      //MainWindow.AppendMessageBox(DecodeDeviceFeedback(response));
    }

    // Print the channel response to the textbox.
    // This means decoding the ant messages.
    static void D0channel0_channelResponse(ANT_Response response)
    {
      // Check event type:
      if (response.responseID == (byte)ANT_ReferenceLibrary.ANTMessageID.RESPONSE_EVENT_0x40)
      {
        switch ((ANT_ReferenceLibrary.ANTEventID)response.messageContents[2])
        {
          case ANT_ReferenceLibrary.ANTEventID.EVENT_RX_FAIL_0x02:
            MainWindow.AppendMessageBox("RX Fail");
            break;
          case ANT_ReferenceLibrary.ANTEventID.EVENT_RX_SEARCH_TIMEOUT_0x01:
            MainWindow.AppendMessageBox("RX Search Timeout");
            break;
          case ANT_ReferenceLibrary.ANTEventID.EVENT_RX_FAIL_GO_TO_SEARCH_0x08:
            MainWindow.AppendMessageBox("RX Fail goto search");
            break;
          default:
            MainWindow.AppendMessageBox($"Event 0x{response.messageContents[2]:X}");
            break;
        }
      }
      //else
      //{
      //  //MainWindow.AppendMessageBox($"Response id = 0x{response.responseID:X}");
      //}
      string display_text = DecodeChannelFeedback(response);
      MainWindow.UpdateVelosenseDataBox(display_text);

      //Add data to setpoint logger, if required:
      if (_on_setpoint)
      {
        //Setpoint_Logger.AddVeloPoints(velo_data);
      }
    }


    //If an event, then get the individual bytes and time from the response.
    static String DecodeDeviceFeedback(ANT_Response response)
    {
      string toDisplay = "Device: ";
      if (response.responseID == (byte)ANT_ReferenceLibrary.ANTMessageID.RESPONSE_EVENT_0x40)
      {
        //We cast the byte to its messageID string and add the channel number byte associated with the message
        toDisplay += (ANT_ReferenceLibrary.ANTMessageID)response.messageContents[1] + ", Ch:" + response.messageContents[0];
        //Check if the eventID shows an error, if it does, show the error message
        if ((ANT_ReferenceLibrary.ANTEventID)response.messageContents[2] != ANT_ReferenceLibrary.ANTEventID.RESPONSE_NO_ERROR_0x00)
          toDisplay += Environment.NewLine + ((ANT_ReferenceLibrary.ANTEventID)response.messageContents[2]).ToString();
      }
      else   //If the message is not an event, we just show the messageID
        toDisplay += ((ANT_ReferenceLibrary.ANTMessageID)response.responseID).ToString();

      //Finally we display the raw byte contents of the response, converting it to hex
      toDisplay += Environment.NewLine + "::" + Convert.ToString(response.responseID, 16) + ", " + BitConverter.ToString(response.messageContents);
      return toDisplay;
    }

    //Decode ant packet into string showing time recieved, and the 8 bytes of the packet in hex format.
    static String DecodeChannelFeedback(ANT_Response response)
    {
      lock (_locker)
      {
        // Check ANT event:

        bool packet_dropped = false;
        if (response.responseID == (byte)ANT_ReferenceLibrary.ANTMessageID.RESPONSE_EVENT_0x40)
        {
          latest_packet_time = response.timeReceived;
          packet_dropped = true;
        }
        else
        {
          byte[] payload = response.getDataPayload();
          velo_data.DecodeFromANT(payload);
          // Log packet:
          Data_logger.LogPacket(response.timeReceived, payload);
        }

        if (packet_dropped)
          packets_dropped++;
        else
          packets_received++;
      }

      return DisplayVeloData(response.timeReceived.ToString("HH:mm:ss.f"));
    }

    // Format data for 
    private static string DisplayVeloData(string timeReceived)
    {

      const int FieldWidthRightAligned = 10;
      float drop_rate = (float)packets_dropped / (packets_dropped + packets_received);

      string display_text = "";
      display_text +=   $"Latest time =       {timeReceived,FieldWidthRightAligned}\n";
      display_text +=   $"Packets received =  {packets_received,FieldWidthRightAligned}\n";
      display_text +=   $"Packets dropped =   {packets_dropped,FieldWidthRightAligned}\n";
      display_text +=   $"Drop rate=          {drop_rate,FieldWidthRightAligned+1:P1}\n";
      if (velo_data.p01_is_init)
      {
        display_text += "Page 1\n";
        display_text += $"  P1 count= {velo_data.p1_count}\n";
        display_text += $"  CdA accum = {velo_data.cda_accum}, Yaw accum = {velo_data.cda_yaw_accum}\n";
        display_text += $"  CdA = {velo_data.cda:F3}, Yaw = {velo_data.cda_yaw:F1}\n";
        display_text += $"  valid cda = {velo_data.valid_cda}, valid yaw = {velo_data.valid_cda_yaw}\n";
      }
      if (velo_data.p18_is_init)
      {
        display_text += "Page 18\n";
        display_text += $"  P18 count = {velo_data.p18_count}, Density = {velo_data.density}\n";
        display_text += $"  pDyn ac = 0x{velo_data.dyn_press_accum:X}, Yaw ac = 0x{velo_data.wind_yaw_accum:X}\n";
        display_text += $"  pDyn = {velo_data.dyn_press:F1}, Yaw = {velo_data.wind_yaw:F1}°\n";
        display_text += $"  valid dyn = {velo_data.valid_dyn_press}, Valid yaw = {velo_data.valid_wind_yaw}\n";
      }
      if (velo_data.p19_is_init)
      {
        display_text += "Page 19\n";
        display_text += $"  usr_pwr = {velo_data.usr_pwr_pct:F1}%, gra_pwr = {velo_data.gra_pwr_pct:F1}%\n";
        display_text += $"  fri_pwr = {velo_data.fri_pwr_pct:F1}%, aer_pwr = {velo_data.aer_pwr_pct:F1}%\n";
        display_text += $"  kin_pwr = {(velo_data.usr_pwr_pct + velo_data.gra_pwr_pct + velo_data.fri_pwr_pct + velo_data.aer_pwr_pct):F1}%, denom = {velo_data.pwr_denom}W\n";
      }
      if (velo_data.p20_is_init)
      {
        display_text += "Page 20\n";
        display_text += $"  P20 count = {velo_data.p20_count}, humidity= {velo_data.humidity}\n";
        display_text += $"  baro acc = 0x{velo_data.baro_accum:X}, temp acc= 0x{velo_data.temp_accum:X}\n";
        display_text += $"  baro = {velo_data.baro}, temp = {velo_data.temp}\n";
        display_text += $"  valid baro = {velo_data.valid_baro}, Valid temp = {velo_data.valid_temp}\n";
      }
      if (velo_data.p80_is_init)
      {
        display_text += "Page 80\n";
        display_text += $"  HW rev = {velo_data.p80_hw_rev}, manf id = {velo_data.p80_manf_id}\n";
        display_text += $"  Model no = {velo_data.p80_model_number}\n";
      }
      if (velo_data.p81_is_init)
      {
        display_text += "Page 81\n";
        display_text += $"  SW revision = {velo_data.p81_software_rev_main}.{velo_data.p81_software_rev_sup}\n";
        display_text += $"  Serial number = {velo_data.p81_serial_number}\n";
      }
      if (velo_data.p82_is_init)
      {
        display_text += "Page 82\n";
        display_text += $"  Bat voltage = {velo_data.p82_voltage:F3}, soc = {velo_data.p82_soc}%\n";
      }
      if (velo_data.p86_is_init)
      {
        display_text += "Page 86\n";
        display_text += $"  Paired device missing = {velo_data.paired_device_missing}\n";
        display_text += $"  Device {velo_data.device_index}/{velo_data.devices_count}\n";
        display_text += $"  Device number = {velo_data.device_number}\n";
        display_text += $"  Transmission type = {velo_data.device_trans_type}\n";
        display_text += $"  Device type = {velo_data.device_type}\n";
      }

      //if (velo_data.p10_received & velo_data.p11_received)
      //{
      //  display_text += "\r\n" + $"Dynamic pressure =  {velo_data.pdyn,FieldWidthRightAligned:F1} Pa";
      //  display_text += "\r\n" + $"Yaw angle =         {velo_data.yaw,FieldWidthRightAligned:F1} °";
      //  display_text += "\r\n" + $"Baro pressure =     {velo_data.baro_cor,FieldWidthRightAligned:F1} Pa";
      //  display_text += "\r\n" + $"Temperature  =      {velo_data.temp,FieldWidthRightAligned:F1} °C";
      //  display_text += "\r\n" + $"Density =           {velo_data.density,FieldWidthRightAligned:F3} kg/m3";
      //  display_text += "\r\n" + $"Airspeed =          {velo_data.airspeed,FieldWidthRightAligned:F1} m/s";
      //}
      //if (velo_data.p12_received)
      //{
      //  display_text += "\r\n" + $"Battery level =     {velo_data.bat_level,FieldWidthRightAligned:F1} %";
      //  display_text += "\r\n" + $"Battery voltage =   {velo_data.bat_voltage,FieldWidthRightAligned:F3} V";
      //}
      return display_text;
    }
    public static void Shutdown()
    {
      //If you need to manually disconnect from a ANTDevice call the shutdownDeviceInstance method,
      //It releases all the resources, the device, and nullifies the object reference
      ANT_Device.shutdownDeviceInstance(ref device0);
    }

    #endregion ANT specific functions

    #region Helper functions

    public static void StartSetpoint()
    {
      lock (_locker)
      {
        _on_setpoint = true;
      }
    }
    public static void EndSetpoint()
    {
      lock (_locker)
      {
        _on_setpoint = false;
      }
    }

    public static void SetpointNumber(int setpoint_no, double angle)
    {
      lock (_locker)
      {
        _setpoint_number = setpoint_no;
        _angle = angle;
      }
    }


    #endregion Helper functions

  }
}
