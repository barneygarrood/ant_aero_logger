﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ANT_Aero_Logger
{
  public class VeloData
  {
    // Page 1:
    public bool p01_is_init;
    public float cda;
    public byte valid_cda;
    public float cda_yaw;
    public byte valid_cda_yaw;
    public UInt16 cda_accum;
    public UInt16 cda_yaw_accum;
    public byte p1_count;
    
    // page 18:
    public bool p18_is_init;
    public byte valid_dyn_press;
    public byte valid_wind_yaw;
    public UInt32 dyn_press_accum;
    public UInt16 wind_yaw_accum;
    public float dyn_press;
    public float wind_yaw;
    public float density;
    public byte p18_count;

    // page 19:
    public bool p19_is_init;
    public sbyte usr_pwr_pct;
    public sbyte gra_pwr_pct;
    public sbyte fri_pwr_pct;
    public sbyte aer_pwr_pct;
    public UInt16 pwr_denom;

    // page 20:
    public bool p20_is_init;
    public byte p20_count;
    public byte valid_baro;
    public byte valid_temp;
    public UInt32 baro_accum;
    public UInt16 temp_accum;
    public byte humidity;
    public float baro;
    public float temp;

    //p80:
    public bool p80_is_init;
    public byte p80_hw_rev;
    public UInt16 p80_manf_id;
    public UInt16 p80_model_number;

    //p81: 
    public bool p81_is_init;
    public byte p81_software_rev_main;
    public byte p81_software_rev_sup;
    public UInt32 p81_serial_number;

    //p82:
    public bool p82_is_init;
    public float p82_voltage;
    public byte p82_soc;

    //p86:
    public bool p86_is_init;
    public bool paired_device_missing;
    public byte devices_count;
    public byte device_index;
    public UInt16 device_number;
    public byte device_trans_type;
    public byte device_type;

    //Initialiser:
    public VeloData()
    {
      Reset();
    }

    public void Reset()
    {
      p01_is_init = false;
      p1_count = 0;
      valid_cda = 0;
      valid_cda_yaw = 0;
      cda_accum = 0;
      cda_yaw_accum = 0;

      p18_is_init = false;
      p18_count = 0;
      dyn_press_accum = 0;
      wind_yaw_accum = 0;
      valid_dyn_press = 0;
      valid_wind_yaw = 0;

      p19_is_init = false;

      p20_is_init = false;
      p20_count = 0;
      baro_accum = 0;
      temp_accum = 0;
      valid_baro = 0;
      valid_temp = 0;

      p86_is_init = false;
      paired_device_missing = false;

    }

    public void DecodeFromANT(byte[] payload)
    {
      byte count;
      switch (payload[0])
      {
        case 1:
          count = (byte)(payload[1] & ((1 << 5) - 1));
          UInt16 cda_accum_new = (UInt16)((payload[3] << 8) | payload[2]);
          UInt16 cda_yaw_accum_new = (UInt16)((payload[5] << 8) | payload[4]);
          if (p01_is_init)
          {
            valid_cda = (byte)((payload[1] >> 5) & 0x01);
            valid_cda_yaw = (byte)((payload[1] >> 6) & 0x01);
            if (valid_cda > 0)
            {
              cda = ((cda_accum_new - cda_accum) & 0xFFFF) / ((count - p1_count) & 0x1F) * 0.0001f;
            }
            if (valid_cda_yaw > 0)
            {
              cda_yaw = ((cda_yaw_accum_new - cda_yaw_accum) & 0xFFFF) / ((count - p1_count) & 0x1F) * 0.05f - 90.0f;
            }
          }
          p1_count = count;
          cda_accum = cda_accum_new;
          cda_yaw_accum = cda_yaw_accum_new;
          p01_is_init = true;
          break;
        case 18:
          count = (byte)(payload[1] & ((1 << 5) - 1));
          density = 0.5f + payload[2] * 0.005f;
          UInt32 dyn_press_accum_new = (UInt32)(payload[5] << 16) | (UInt32)(payload[4] << 8) | (UInt32)payload[3];
          UInt16 wind_yaw_accum_new = (UInt16)((payload[7] << 8) | payload[6]);
          if (p18_is_init)
          {
            valid_dyn_press = (byte)((payload[1] >> 6) & 0x01);
            valid_wind_yaw = (byte)((payload[1] >> 7) & 0x01);
            if (valid_dyn_press > 0)
            {
              dyn_press = ((dyn_press_accum_new - dyn_press_accum) & 0xFFFFFF) / ((count - p18_count) & 0x1F) * 0.001f;
            }
            if (valid_wind_yaw > 0)
            {
              wind_yaw = ((wind_yaw_accum_new - wind_yaw_accum) & 0xFFFF) / ((count - p18_count) & 0x1F) * 0.05f - 90.0f;
            }
          }
          p18_count = count;
          dyn_press_accum = dyn_press_accum_new;
          wind_yaw_accum = wind_yaw_accum_new;
          p18_is_init = true;
          break;
        case 19:
          usr_pwr_pct = (sbyte)payload[2];
          gra_pwr_pct = (sbyte)payload[3];
          fri_pwr_pct = (sbyte)payload[4];
          aer_pwr_pct = (sbyte)payload[5];
          pwr_denom = (UInt16)((payload[7] << 8) | payload[6]);
          p19_is_init = true;
          paired_device_missing = false;
          break;
        case 20:
          count = (byte)(payload[1] & ((1 << 5) - 1));
          UInt32 baro_accum_new = (UInt32)((payload[4] << 16) | (payload[3] << 8) | payload[2]);
          UInt16 temp_accum_new = (UInt16)((payload[6] << 8) | payload[5]);
          if (p20_is_init)
          {
            valid_baro = (byte)((payload[1] >> 6) & 0x01);
            valid_temp = (byte)((payload[1] >> 7) & 0x01);
            if (valid_baro>0)
            {
              baro = 108000.0f - ((baro_accum_new - baro_accum) & 0xFFFFFF) / ((count - p20_count) & 0x1F) * 0.05f;
            }
            if (valid_temp>0)
            {
              temp = ((temp_accum_new - temp_accum) & 0xFFFF) / ((count - p20_count) & 0x1F) * 0.01f-30.0f;
            }
          }
          humidity = payload[7];
          p20_count = count;
          baro_accum = baro_accum_new;
          temp_accum = temp_accum_new;
          p20_is_init = true;
          break;
        case 80:
          p80_is_init = true;
          p80_hw_rev = payload[3];
          p80_manf_id = (UInt16)((payload[5]<<8) | (payload[4]));
          p80_model_number = (UInt16)((payload[7] << 8) | (payload[6]));
          break;
        case 81:
          p81_is_init = true;
          p81_software_rev_sup = payload[2];
          p81_software_rev_main = payload[3];
          p81_serial_number = (UInt32)(payload[7] << 24 | payload[6] << 16 | payload[5] << 8 | payload[4]);
          break;
        case 82:
          p82_is_init = true;
          p82_voltage = payload[6] / 256f + (float)(payload[7] & 0xF);
          p82_soc = (byte)(90 - ((payload[7] >> 4 & 0x07) - 1) * 20);
          break;
        case 86:
          p86_is_init = true;
          paired_device_missing = true;
          devices_count = payload[2];
          device_index = payload[1];
          device_number = (UInt16)((payload[5] << 8) | payload[4]);
          device_trans_type = payload[6];
          device_type = payload[7];
          break;
      }
    }
  }
}
